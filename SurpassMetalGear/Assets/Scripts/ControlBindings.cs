﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBindings : MonoBehaviour
{
    /************************************************************************************************
                                     Keyboard Inputs
    ************************************************************************************************/
    public KeyCode melee; //unused
    public KeyCode projectile;
    public KeyCode makeNoise;
}
