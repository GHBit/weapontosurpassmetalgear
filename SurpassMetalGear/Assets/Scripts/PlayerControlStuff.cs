﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlStuff : Controller
{
    public PlayerPawnStuff playerPawn;
    public Manager gameManager;

    // Use this for initialization
    void Start()
    {
        playerPawn = gameObject.GetComponent<PlayerPawnStuff>();
        gameManager = Manager.me;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerPawn != null)
        {
            //movement controls
            if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
            {

                playerPawn.Rotate(new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0)); //rotates based on vertical and horizontal axes
                playerPawn.MoveForward(); //and moves in that direction

            }

            if (gameManager.controls != null)
            {
                //make a loud sound
                if (Input.GetKey(gameManager.controls.makeNoise))
                {
                    playerPawn.MakeLoudNoise();
                }

                //shoots a bullet
                if (Input.GetKeyDown(gameManager.controls.projectile))
                {
                    playerPawn.ShootBullet();
                }
            }
        }

    }
}
