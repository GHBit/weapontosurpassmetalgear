﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewCone : MonoBehaviour
{
    public float fovAngle;
    public float lineOfSightDistance;
    public float distanceToStartRaycastSearch;
    public GameObject[] objectsToLookFor;

    //looks for whether or not the AI can see something
    public GameObject CanSeeSomething()
    {
        foreach (GameObject objectToLookFor in objectsToLookFor) //for everything the AI is looking for
        {
            Vector3 positionWhenLookingAtTarget = objectToLookFor.transform.position - transform.position; //value used to calculate angle between target and self
            float angleToLookAtTarget = Vector3.Angle(positionWhenLookingAtTarget, transform.up); //calculates said angle

            if (angleToLookAtTarget < fovAngle) //if the angle is within the FOV
            {
                RaycastHit2D raycastSearch = Physics2D.Raycast(transform.position + transform.up * distanceToStartRaycastSearch, positionWhenLookingAtTarget); //performs a raycast

                if (raycastSearch.collider.gameObject == objectToLookFor && Vector3.Distance(objectToLookFor.transform.position, transform.position) < lineOfSightDistance) //if the player is the first object hit by the raycast and is also close enough to the AI
                {
                    return objectToLookFor; //returns the object
                }
            }
        }

        return gameObject; //if none of the objects are found it returns itself
    }
}