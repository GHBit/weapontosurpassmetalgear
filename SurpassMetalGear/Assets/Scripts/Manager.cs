﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public static Manager me;
    //General
    public GameObject player;
    public GameObject mainCamera;
    public GameObject[] aiCharacters;
    public ControlBindings controls;
    public float soundDegradationWithDistance;
    public GameStates gameState;
    public Canvas gameplayCanvas;
    public Canvas gameOverCanvas;

    public enum GameStates { TitleScreen, Gameplay, GameOver };

    // Use this for initialization
    void Start()
    {
        controls = GetComponent<ControlBindings>();
    }

    //Next 2 methods ensure that Game Manager continues to work properly after transition between scenes
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLoad;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLoad;
    }

    //Tells the game manager what to grab when loading a scene
    private void OnLoad(Scene scene, LoadSceneMode mode)
    {
        player = GameObject.Find("Player Character");
        mainCamera = GameObject.Find("Main Camera");
        aiCharacters = GameObject.FindGameObjectsWithTag("AI");
        gameplayCanvas = GameObject.Find("GameplayCanvas").GetComponent<Canvas>();
        gameOverCanvas = GameObject.Find("GameOverCanvas").GetComponent<Canvas>();



        //if the scene is the title screen, set mode to title screen. Otherwise set mode to gameplay
        if (scene.name == "TitleScreen")
        {
            gameState = GameStates.TitleScreen;
        }
        else
        {
            gameState = GameStates.Gameplay;
        }
    }

    private void Awake()
    {
        if (me == null)
        {
            me = this; //assigns game manager in usable form.
            DontDestroyOnLoad(gameObject); //preserves object between scenes
        }
        else
        {
            Destroy(this.gameObject); //destroys any extra managers
            Debug.Log("Game Manager already exists");
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (gameState)
        {
            case GameStates.TitleScreen: //if the game mode is title screen then nothing needs to be done. State changes are done when the scene loads in OnLoad
                break;
            case GameStates.Gameplay: //if the game mode is gameplay then it will play its normal functions
                //Makes Camera track player
                if (mainCamera != null && player != null)
                {
                    mainCamera.transform.position = player.transform.position - new Vector3(0, 0, 10);
                }

                //changes to game over state is the player is gone
                if (player == null)
                {
                    gameState = GameStates.GameOver;
                    gameOverCanvas.enabled = true;
                    gameplayCanvas.enabled = false;
                }
                else //or if all the AI are gone
                {
                    bool allAIKilled = true;
                    foreach (GameObject aiCharacter in aiCharacters)
                    {
                        if (aiCharacter != null)
                        {
                            allAIKilled = false;
                        }
                    }

                    if (allAIKilled == true)
                    {
                        gameState = GameStates.GameOver;
                        gameOverCanvas.enabled = true;
                        gameplayCanvas.enabled = false;
                    }
                }

                break;
            case GameStates.GameOver: //UI pops up during state change. Either scene is reloaded or title screen scene is loaded via buttons on canvas, so nothing happens here
                break;
        };
    }
}
