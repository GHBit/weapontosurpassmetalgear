﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public GameObject parent;
    public float movementSpeed;
    public float duration;
    public float timer = 0;

    //Update is called once per frame
    void Update () {
        transform.position += transform.up * movementSpeed * Time.deltaTime; //moves bullet
        timer += Time.deltaTime; //increments timer

        if (timer > duration)
        {
            Destroy(gameObject); //and destroys bullet if timer goes for too long
        }
	}
}
