﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Listener : MonoBehaviour {
    public NoiseMaker[] noiseMakersToAcknowledge; //this is an array so I can add additional noise makers to look for more easily
    public Manager gameManager;

    private void Start()
    {
        gameManager = Manager.me;
    }

    //tells another component if it can hear a noise maker
    public Vector2 CanIHearThePlayer()
    {
        foreach (NoiseMaker noiseMaker in noiseMakersToAcknowledge) //for every noise maker the listener looks for
        {
            if (noiseMaker != null) //if there is a noise maker
            {
                if (noiseMaker.currentNoiseVolume > Vector2.Distance(transform.position, noiseMaker.transform.position)) //and the distance from it is less than the volume of the noise
                {
                    return noiseMaker.transform.position; //return the noise maker's position
                }
            }
        }

        return transform.position; //otherwise return the current object's position
    }
}
