﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPawnStuff : Pawn
{
    public Listener hiIListenToNoise;
    public ViewCone hiILookForStuff;

    // Use this for initialization
    void Start()
    {
        hiIListenToNoise = gameObject.GetComponent<Listener>();
        hiILookForStuff = gameObject.GetComponentInChildren<ViewCone>();
    }

    public override void Rotate(Vector3 vector)
    {
        transform.up = (vector - transform.position);
    }

    //fixed rotation for default state
    public void RotateAuto(Vector3 vector)
    {
        transform.Rotate(vector);
    }

    //listen for loud noise
    public Vector2 ListenForLoudNoise()
    {
        if (hiIListenToNoise != null)
        {
            return hiIListenToNoise.CanIHearThePlayer(); //returns vector position from listener
        }
        return transform.position; //or position of self if not present
    }

    //looks for an object in a view cone
    public GameObject LookForSomething()
    {
        if (hiILookForStuff != null)
        {
            return hiILookForStuff.CanSeeSomething();
        }
        return gameObject;
    }
}
