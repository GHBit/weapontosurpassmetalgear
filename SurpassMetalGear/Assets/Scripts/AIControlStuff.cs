﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControlStuff : Controller
{
    public enum AIStates { Default, SoundAlert, SightAlert }

    public AIPawnStuff AIPawn;
    public AIStates aiState;
    public Vector3 locationOfDetectedSound;
    public Vector3 distanceToBeCloseEnoughToSound;
    public GameObject objectISaw;

    // Use this for initialization
    void Start()
    {
        AIPawn = gameObject.GetComponent<AIPawnStuff>();
    }

    // Update is called once per frame
    void Update()
    {
        //FSM
        switch (aiState)
        {
            case AIStates.Default:
                //Actions
                if (AIPawn != null)
                {
                    AIPawn.RotateAuto(new Vector3(0, 0, 1));
                }

                //State Changes
                locationOfDetectedSound = AIPawn.ListenForLoudNoise(); //sets the location of the most recently detected sound
                if (locationOfDetectedSound != transform.position) //if the value that ended up being set there is not the AI's current position
                {
                    aiState = AIStates.SoundAlert; //State change
                }

                Vision(); //potentially sets AI mode to SightAlert
                break;
            case AIStates.SoundAlert:
                //Actions
                AIPawn.Rotate(locationOfDetectedSound); //look at where the sound came from
                AIPawn.MoveForward(); //and move towards it

                //State Changes
                float xDistance = (transform.position - locationOfDetectedSound).x; //grabs distance from sound position. This is done because greater than and less than operations cannot be applied to vectors.
                float yDistance = (transform.position - locationOfDetectedSound).y;
                if (Mathf.Abs(xDistance) < distanceToBeCloseEnoughToSound.x && Mathf.Abs(yDistance) < distanceToBeCloseEnoughToSound.y) //if the AI is within a certain distance of the sound's source
                {
                    aiState = AIStates.Default; //it will go back to its default state
                }

                Vision(); //potentially sets AI mode to SightAlert
                break;
            case AIStates.SightAlert:
                //Actions
                if (objectISaw != null)
                {
                    AIPawn.Rotate(objectISaw.transform.position); //rotate towards detected object
                    AIPawn.MoveForward(); //and move towards it
                    AIPawn.ShootBullet();
                }

                //State Changes
                if (objectISaw == null) //if the target is dead
                {
                    aiState = AIStates.Default; //reset to default
                }
                break;
        }
    }

    //method looks for anything the AI sees
    private void Vision()
    {
        objectISaw = AIPawn.LookForSomething(); //Gets the object the view cone saw. If nothing it will get the AI
        if (objectISaw != gameObject) //if it doesn't get the AI
        {
            aiState = AIStates.SightAlert; //goes into sight alert
        }
    }
}
