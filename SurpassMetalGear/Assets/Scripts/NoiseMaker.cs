﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour {

    public float currentNoiseVolume;

    private void Update()
    {
        if (currentNoiseVolume < 0)
        {
            currentNoiseVolume = 0;
        }
        else if (currentNoiseVolume > 0)
        {
            currentNoiseVolume -= Manager.me.soundDegradationWithDistance * Time.deltaTime;
        }
    }
}
