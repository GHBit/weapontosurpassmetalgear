﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Pawn : MonoBehaviour {
    public float movementSpeed;
    public float turningSpeed;
    public float loudNoiseVolume;
    public float health;
    public float maxHealth;
    public float damageTakenFromBullet;
    public Bullet bullet;
    public float bulletOffset;
    public Slider healthSlider;

    private void Start()
    {
        if (bullet != null)
        {
            bullet.parent = gameObject;
        }
    }

    public void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }

        if (healthSlider != null)
        {
            healthSlider.value = health / maxHealth * 100;
        }
    }

    public virtual void Rotate(Vector3 vector)
    {
        //default behavior, intended to be overwritten
        transform.Rotate(vector);
    }

    //moves pawn forward at framerate independant speed
    public void MoveForward()
    {
        transform.position += transform.up * movementSpeed * Time.deltaTime;
    }

    //shoots a bullet forward
    public void ShootBullet()
    {
        if (bullet != null)
        {
            Instantiate(bullet, transform.position + transform.up * bulletOffset, transform.rotation);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            health -= damageTakenFromBullet;
        }
    }
}
