﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //needed for Button class and other UI related tasks
using UnityEngine.SceneManagement; //needed for Scene transitions

public class StartButton : MonoBehaviour {
    public Manager gameManager;
    public string nameOfSceneToLoad;

    private void Start()
    {
        gameManager = Manager.me;
    }

    //when the button is clicked, move to Gameplay scene
    public void DoClick()
    {
        SceneManager.LoadScene(nameOfSceneToLoad);
    }
}
