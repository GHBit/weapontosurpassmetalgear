﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPawnStuff : Pawn {
    public NoiseMaker hiIMakeNoise;
    public Text healthTextBox;
    public Sprite healthyImage;
    public Sprite dangerImage;
    public Image stateInfoOnUI;

    private void Start()
    {
        hiIMakeNoise = gameObject.GetComponent<NoiseMaker>();
    }

    //updates HUD display
    new private void Update()
    {
        base.Update();
        healthTextBox.text = health.ToString();

        if (health > 20)
        {
            stateInfoOnUI.sprite = healthyImage;
        }
        else
        {
            stateInfoOnUI.sprite = dangerImage;
        }
    }

    //rotates pawn
    public override void Rotate(Vector3 vector)
    {
        transform.up += (transform.position + vector - transform.position) * turningSpeed * Time.deltaTime; //moves angle of pawn towards axes
    }

    //make a loud noise
    public void MakeLoudNoise()
    {
        if (hiIMakeNoise != null)
        {
            hiIMakeNoise.currentNoiseVolume = loudNoiseVolume;
        }
    }
}
